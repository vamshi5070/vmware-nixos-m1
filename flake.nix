{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    #inputs.
#    vscode-server.url = "github:msteen/nixos-vscode-server";
#     vscode-server = "https://github.com/msteen/nixos-vscode-server/tarball/master"}/modules/vscode-server/home.nix";
  };
  outputs = inputs@{ self, nixpkgs, ... }:
    let
      system = "aarch64-linux";
      myPkgs = import nixpkgs {
        inherit system;
        #config = 
        config = {
          allowUnfree = true;
          # allowBroken = true;
          allowUnsupportedSystem = true;
        };
      };

    in {
      homeConfigurations = {
        cosmos = inputs.home-manager.lib.homeManagerConfiguration {
          pkgs = myPkgs; # nixpkgs.legacyPackages.${system};
          modules = [
            {
              programs.htop.enable = true;
}
/*
              programs.starship  = {
			enable = true;
			enableFishIntegration = true;
		};
            }
*/
	./git
        ./starship
        ./fish
./kitty
#                         vscode-server.nixosModule
#                           ({ config, pkgs, ... }: {
#                                  services.vscode-server.enable = true;})
            #./yi
            #		./brickFileTree
            # ./taskell
/* 
           ./helix
            # ./tree
            ./kitty
            ./neovim
            ./fish
            ./starship
            ./direnv
            ./git
            ./emacs
            ./alacritty
            ./hledger
            # ./texlive
            ./pdflatex
            ./pandoc
*/  
          {
              programs.home-manager.enable = true;
              home = {
                username = "vamshi";
                homeDirectory = "/home/vamshi";
                stateVersion = "22.11";
              };
            }

          ];

          #};
        };
      };
    };
}
