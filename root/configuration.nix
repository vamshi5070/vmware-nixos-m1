# Edit this configuration file to define what should be installed on your system.  Help is available in the configuration.nix(5) man 
# page and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{ imports = [ # Include the results of the hardware scan.
      ./hardware-configuration.nix ./modules/vmware-guest.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true; boot.loader.efi.canTouchEfiVariables = true;

  boot.loader.systemd-boot.consoleMode = "0";

  disabledModules = [ "virtualisation/vmware-guest.nix" ];

  # This works through our custom module imported above
  virtualisation.vmware.guest.enable = true;


 # Don't require password for sudo
  security.sudo.wheelNeedsPassword = false;

  networking.hostName = "cosmos"; # Define your hostname.
  # Pick only one of the below networking options. networking.wireless.enable = true; # Enables wireless support via wpa_supplicant. 
  # networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.

  # Set your time zone. time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary networking.proxy.default = "http://user:password@proxy:port/"; networking.proxy.noProxy = 
  # "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
 # i18n.defaultLocale = "en_US.UTF-8";

  # Select internationalisation properties. i18n.defaultLocale = "en_US.UTF-8"; console = { font = "Lat2-Terminus16"; keyMap = "us"; 
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system. services.xserver.enable = true; nix = { e

  services = {
    #spice-vdagentd.enable = true; # vm (utm) spice-webdavd.enable = true; # vm (utm) openssh.enable = true;
    xserver = { enable = true;
  #layout = "us";
      dpi = 220; windowManager = { xmonad = {
          enable = true; enableContribAndExtras = true;
          #config = ./xmonad.hs;
        };
      };
      displayManager = {
       # AARCH64: For now, on Apple Silicon, we must manually set the
      # display resolution. This is a known issue with VMware Fusion.
      sessionCommands = '' ${pkgs.xorg.xset}/bin/xset r rate 200 40 '';
#sessionCommands = ''
 #  ${pkgs.xorg.xrandr}/bin/xrandr --newmode 1080x1920_60.00 176.50 1080 1168 1280 1480 1920 1923 1933 1989 -hsync +vsync ; xrandr 
 #  --addmode Virtual-1 1080x1920_60.00 ; xrandr --output Virtual-1 --mode 1080x1920_60.00
#                ''; ${pkgs.xorg.xrandr}/bin/xrandr -s '1920x1080'
        defaultSession = "none+xmonad"; autoLogin = { enable = true; user = "vamshi";
        };
      };
    };
  };

  

  # Configure keymap in X11 services.xserver.layout = "us"; services.xserver.xkbOptions = { "eurosign:e"; "caps:escape" # map caps to 
  #   escape.
  # };

  # Enable CUPS to print documents. services.printing.enable = true;

  # Enable sound. sound.enable = true; hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager). services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = { isNormalUser = true; extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user. shell = pkgs.fish;
     packages = with pkgs; [
  #     firefox thunderbird
  neofetch htop

   ];
  };
 # Be careful updating this.
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # List packages installed in system profile. To search, run: $ nix search wget
   environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default. wget
 # Be careful updating this.
#  boot.kernelPackages = pkgs.linuxPackages_latest;
firefox alacritty emacs dmenu
    # For hypervisors that support auto-resizing, this script forces it. I've noticed not everyone listens to the udev events so this 
    # is a hack.
    (writeShellScriptBin "xrandr-auto" '' xrandr --output Virtual-1 --auto '')
 # xrandr --output Virtual-2 --auto --output Virtual-1 --left-of Virtual-2
   #  '')
 ];

  # Some programs need SUID wrappers, can be configured further or are started in user sessions. programs.mtr.enable = true; 
  # programs.gnupg.agent = {
  #   enable = true; enableSSHSupport = true;
  # };


  fonts.fonts= with pkgs; [ fira-code ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall. networking.firewall.allowedTCPPorts = [ ... ]; networking.firewall.allowedUDPPorts = [ ... ]; Or 
  # disable the firewall altogether. networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system (/run/current-system/configuration.nix). This is useful in 
  # case you accidentally delete configuration.nix. system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default settings for stateful data, like file locations and database 
  # versions on your system were taken. It‘s perfectly fine and recommended to leave this value at the release version of the first 
  # install of this system. Before changing this value read the documentation for this option (e.g. man configuration.nix or on 
  # https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}
