{...}:{
  programs.fish = {
    enable = true;
    shellInit = "
    export PATH=\"$HOME/.emacs.d/bin:$PATH\"
    export PATH=\"$HOME/.cargo/bin:$PATH\"
    set fish_greeting";

  };
}

